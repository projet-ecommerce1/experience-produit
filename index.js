const express = require('express')
const mongoose = require('mongoose')

const MONGO_URL = process.env.MONGO_URL || 'mongodb://mongodb:27010';
const PORT = process.env.PORT || 4000;

mongoose.Promise = global.Promise;

mongoose.connect(`${MONGO_URL}/produit-service`)
.then(()=>{
    console.log(`Successfully connected to db`);
})
.catch(err=>{
    console.log(`Could not connect to db`, err);
    process.exit();
});

const app = express();
app.use(express.json());

const produitController = require('./controller/produitController');
app.use(produitController);
console.log('Start service on port : ', PORT);
app.listen(PORT);