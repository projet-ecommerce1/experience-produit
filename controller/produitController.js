const express = require('express')
const Produit = require('../model/produit')

var router = express.Router();

router.post('/ajouter', function name(req, res) {
    const {nom, description, prix } = req.body;
    const produit = new Produit({nom, description, prix});

    produit.save()
    .then(data =>{
        res.send(data);
    })
    .catch(err => {
        res.status('500').send({'message':`error creating product ${err.message}`});
    });

});
router.get('/acheter', function name(req,res) {
    Produit.find()
    .then(data =>{
        res.send(data);
    })
    .catch(err => {
        res.status('500').send({'message':`cannot find products ${err.message}`});
    });

});


module.exports = router